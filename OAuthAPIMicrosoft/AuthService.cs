﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Net.Http;
using System.Web;
using Newtonsoft.Json.Linq;

namespace OAuthAPIMicrosoft
{
    public class AuthService
    {
        public AuthService()
        {
        }

        public string GetAccessToken(string authCode)
        {
           string url  ="https://login.microsoftonline.com/organizations/oauth2/v2.0/token";

            var values = new Dictionary<string, string>
            {
                { "client_id", "88d62043-58d6-4277-990b-a04eabfc3974" },
                { "code", authCode },
                { "redirect_uri", "https://localhost:5001/api/oauth"},
                {"scope","openid profile email" },
                {"grant_type", "authorization_code" },
                {"client_secret", "hkj_w4~K3x1Q~F.8xj79wD1~-NTKHa4Wbe" }

            };
            var content = new FormUrlEncodedContent(values);
       
            try
            {
                using (HttpClient h = new HttpClient())
                {

                    var result = h.PostAsync(url, content).Result;
                    return result.Content.ReadAsStringAsync().Result;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public string DecodeToken(string token)
        {
            dynamic data = JObject.Parse(token);
            
            var stream = data.access_token;
            var handler = new JwtSecurityTokenHandler();
            var jsonToken = handler.ReadToken(stream);
            var tokenS = handler.ReadToken(stream) as JwtSecurityToken;
            //return client id
            return tokenS.Payload.Sub;
        }
    }
}
