﻿using System;
namespace OAuthAPIMicrosoft.Product
{
    public class OAuthAuthProvider : AuthProvider
    {
        public string Audience { get; set; }

        public OAuthAuthProvider(string clientId, string clientSecret, string audience, string grantType, string url, string returnUrl)
        {
            ClientId = clientId;
            ClientSecret = clientSecret;
            Audience = audience;
            GrantType = grantType;
            BaseUrl = url;
            ReturnUrl = returnUrl;
            
        }
}
}
