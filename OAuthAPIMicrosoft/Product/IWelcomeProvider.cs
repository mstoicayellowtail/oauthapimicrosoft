﻿using System;
namespace OAuthAPIMicrosoft.Product
{
    public class IWelcomeProvider: AuthProvider
    {

        public IWelcomeProvider(string clientId, string clientSecret, string grantType, string url, string returnUrl)
        {
            ClientId = clientId;
            ClientSecret = clientSecret;
            GrantType = grantType;
            BaseUrl = url;
            ReturnUrl = returnUrl;

        }
    }
}
