﻿using System;
namespace OAuthAPIMicrosoft.Product
{
    //the "Product" abstract class
    public abstract class AuthProvider
    {
        public string BaseUrl { get; set; }
        public string ClientId { get; set; }
        public string ReturnUrl { get; set; }
        public string GrantType { get; set; }
        public string ClientSecret { get; set; }

    }
}
