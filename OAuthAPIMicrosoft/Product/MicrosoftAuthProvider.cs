﻿using System;
namespace OAuthAPIMicrosoft.Product
{
    public class MicrosoftAuthProvider : AuthProvider
    {
        public string Scope { get; set; }

        public MicrosoftAuthProvider(string clientId, string clientSecret, string scope, string grantType, string url, string returnUrl)
        {
            ClientId = clientId;
            ClientSecret = clientSecret;
            Scope = scope;
            GrantType = grantType;
            BaseUrl = url;
            ReturnUrl = returnUrl;
            
        }
    }
}
