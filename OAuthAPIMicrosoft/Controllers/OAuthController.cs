﻿using System;
using Microsoft.AspNetCore.Mvc;
using OAuthAPIMicrosoft.Factory;

namespace OAuthAPIMicrosoft.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OAuthController : ControllerBase
    {
        //private AuthService AuthService;

        public OAuthController()
        {
        }
        // GET api/values
        [HttpGet]
        public IActionResult Get([FromQuery(Name="code")] string code)
        {
            string provider = "iwelcome";
            AuthFactory factory;

            if(provider == "azure")
            {
                factory = new MicrosoftFactory();
                string token = factory.GetAccessToken(code);
                return Ok("Code is: " + code + " and response is: " + token + Environment.NewLine + "token sub is: ");

            }

            if (provider == "auth")
            {
                factory = new OAuthFactory();
                string token = factory.GetAccessToken();
                return Ok("Code is: " + code + " and response is: " + token + Environment.NewLine + "token sub is: ");

            }
            if (provider == "iwelcome")
            {
                factory = new IWelcomeFactory();
                string token = factory.GetAccessToken(code);
                return Ok("Code is: " + code + " and response is: " + token + Environment.NewLine + "token sub is: ");

            }
            return NoContent();
        }

        [HttpGet("url/{type}")]
        public IActionResult GetAuthURL(string type)
        {
            if (type == "azure")
            {
                string url = "https://login.microsoftonline.com:443/organizations/oauth2/v2.0/authorize?client_id=88d62043-58d6-4277-990b-a04eabfc3974&response_type=code&redirect_uri=https%3a%2f%2flocalhost%3a5001%2fapi%2foauth&response_mode=query&scope=openid&state=12345&nonce=678910";
                return Ok(url);
            }
            if (type == "iwelcome")
            {
                string url = "https://asr.iwelcome.com/ivl/login?sessionOnly=true&goto=https%3A%2F%2Fasr.iwelcome.com%2Fivl%2Fauth%2Foauth2.0%2Fv1%2Fauthorize%3Fresponse_type%3Dcode%26redirect_uri%3Dhttps%3A%2F%2Flocalhost%3A5001%2Fapi%2Foauth%26client_id%3Divl%26scope%3Dopenid%2520profile%26state%3D123%26prompt%3D";
                return Ok(url);
            }
            return NotFound();
        }


    }
}
