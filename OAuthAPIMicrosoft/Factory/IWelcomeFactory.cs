﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using OAuthAPIMicrosoft.Product;

namespace OAuthAPIMicrosoft.Factory
{
    public class IWelcomeFactory : AuthFactory
    {
        public IWelcomeFactory()
        {
        }

        public override string GetAccessToken(string code = null)
        {
            var provider = new IWelcomeProvider("ivl", "tgkMem69xf9nDi4K527ekveodRJZLZgAhFfmHKik","authorization_code",
                             "https://asr.iwelcome.com/ivl/auth/oauth2.0/v1/token", "https://localhost:5001/api/oauth");

            string url = provider.BaseUrl;

            var values = new Dictionary<string, string>
            {
                { "client_id",provider.ClientId },
                { "code", code },
                { "redirect_uri", provider.ReturnUrl},
                {"grant_type", provider.GrantType},
                {"client_secret", provider.ClientSecret }

            };
            var content = new FormUrlEncodedContent(values);

            try
            {
                using (HttpClient h = new HttpClient())
                {

                    var result = h.PostAsync(url, content).Result;
                    return result.Content.ReadAsStringAsync().Result;
                }
            }
            catch (Exception e)
            {
                throw e;
            }

        }
    }
}
