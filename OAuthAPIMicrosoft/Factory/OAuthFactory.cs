﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using OAuthAPIMicrosoft.Product;

namespace OAuthAPIMicrosoft.Factory
{
    public class OAuthFactory : AuthFactory
    {
        public OAuthFactory()
        {
        }

        public override string GetAccessToken(string code = null)
        {
            var provider = new OAuthAuthProvider("A0bxF9gh1M0MT9Rw3GRI5e3XaGKbQjcU", "SjQ65hd6ugHdgGKS4NPteADOOiZGzTE8RcdPVyzoolaQgqiiP3gdzSy_DrC2KUsG", "http://oauthtest/api",
                            "client_credentials", "https://dev-ltu78b3o.eu.auth0.com/oauth/token", "https://localhost:5001/api/oauth");

            string url = provider.BaseUrl;

            var values = new Dictionary<string, string>
            {
                { "client_id",provider.ClientId },
                { "code", code },
                { "redirect_uri", provider.ReturnUrl},
                {"audience",provider.Audience},
                {"grant_type", provider.GrantType},
                {"client_secret", provider.ClientSecret }

            };
            var content = new FormUrlEncodedContent(values);

            try
            {
                using (HttpClient h = new HttpClient())
                {

                    var result = h.PostAsync(url, content).Result;
                    return result.Content.ReadAsStringAsync().Result;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}

