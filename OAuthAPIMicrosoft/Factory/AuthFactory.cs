﻿using System;
using OAuthAPIMicrosoft.Product;

namespace OAuthAPIMicrosoft.Factory
{
    public abstract class AuthFactory
    {
        public abstract string GetAccessToken(string code = null);

    }
}
