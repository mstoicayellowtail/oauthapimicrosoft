﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using OAuthAPIMicrosoft.Product;

namespace OAuthAPIMicrosoft.Factory
{
    public class MicrosoftFactory : AuthFactory
    {
        public MicrosoftFactory()
        {
        }

        public override string GetAccessToken(string code)
        {
            var provider = new MicrosoftAuthProvider("88d62043-58d6-4277-990b-a04eabfc3974", "hkj_w4~K3x1Q~F.8xj79wD1~-NTKHa4Wbe", "openid profile email",
                "authorization_code", "https://login.microsoftonline.com/organizations/oauth2/v2.0/token", "https://localhost:5001/api/oauth");
            string url = provider.BaseUrl;

            var values = new Dictionary<string, string>
            {
                { "client_id",provider.ClientId },
                { "code", code },
                { "redirect_uri", provider.ReturnUrl},
                {"scope",provider.Scope},
                {"grant_type", provider.GrantType},
                {"client_secret", provider.ClientSecret }

            };
            var content = new FormUrlEncodedContent(values);

            try
            {
                using (HttpClient h = new HttpClient())
                {

                    var result = h.PostAsync(url, content).Result;
                    return result.Content.ReadAsStringAsync().Result;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}

