FROM mcr.microsoft.com/dotnet/core/sdk:2.2 AS build-env
WORKDIR /app

# Copy csproj and restore as distinct layers
COPY OAuthAPIMicrosoft/OAuthAPIMicrosoft.csproj OAuthAPIMicrosoft/
COPY OAuthAPIMicrosoft/NuGet.config OAuthAPIMicrosoft/

RUN dotnet restore OAuthAPIMicrosoft/OAuthAPIMicrosoft.csproj --configfile OAuthAPIMicrosoft/NuGet.config
#RUN dotnet list OAuthAPIMicrosoft/OAuthAPIMicrosoft.csproj package

# Copy everything else and build
COPY ./ ./
RUN find -type d -name bin -prune -exec rm -rf {} \; && find -type d -name obj -prune -exec rm -rf {} \;
RUN dotnet build 

# Build runtime image


